# author: Carolina de Senne Garcia
# RA 145656
# c145656@dac.unicamp.br
# desennecarol@gmail.com

# Run this thing:
# python3 project.py < input.file

# Description:
# https://www.ic.unicamp.br/~wainer/cursos/2s2019/346/proj-python.html

from collections import deque, namedtuple
import random

# define Dijsktra's Algorithm for a Graph instance
# Dijkstra's Algorithm modified from following source:
# https://dev.to/mxl/dijkstras-algorithm-in-python-algorithms-for-beginners-dkc
inf = float('inf')
Edge = namedtuple('Edge', 'start, end, cost')

def make_edge(start, end, cost=1):
    return Edge(start, end, cost)

class Graph:
    '''
    create a Graph:
    graph = Graph([
    ("a", "b", 7),  ("a", "c", 9),  ("a", "f", 14), ("b", "c", 10),
    ("b", "d", 15), ("c", "d", 11), ("c", "f", 2),  ("d", "e", 6),
    ("e", "f", 9)])

    call dijkstra on it:
    print(graph.dijkstra("a", "e"))
    '''
    def __init__(self, edges):
        self.edges = [make_edge(*edge) for edge in edges]

    @property
    def vertices(self):
        return set(sum(([edge.start, edge.end] for edge in self.edges),[]))
    
    @property
    def neighbours(self):
        neighbours = {vertex: set() for vertex in self.vertices}
        for edge in self.edges:
            neighbours[edge.start].add((edge.end, edge.cost))
        return neighbours

    def remove_edge(self, n1, n2):
        edges = self.edges[:]
        for edge in edges:
            if [edge.start, edge.end] in [[n1,n2]]:
                self.edges.remove(edge)

    def dijkstra(self, source, dest):
        # initialize distances as infinity for all nodes
        distances = {vertex: inf for vertex in self.vertices}
        previous_vertices = {vertex: None for vertex in self.vertices}
        # initialize distance to source as 0
        distances[source] = 0
        vertices = self.vertices.copy()

        while vertices:
            current_vertex = min(vertices, key=lambda vertex: distances[vertex])
            vertices.remove(current_vertex)
            # there is no path from source to dest
            if distances[current_vertex] == inf:
                break
            # update distance to source for neighbours
            for neighbour, cost in self.neighbours[current_vertex]:
                alternative_route = distances[current_vertex] + cost
                if alternative_route < distances[neighbour]:
                    distances[neighbour] = alternative_route
                    previous_vertices[neighbour] = current_vertex
        # calculate path
        path, current_vertex = deque(), dest
        while previous_vertices[current_vertex] is not None:
            path.appendleft(current_vertex)
            current_vertex = previous_vertices[current_vertex]
        if path:
            path.appendleft(current_vertex)
        # return total cost and path
        return distances[dest], list(path)

DistanceEdge = namedtuple('distanceEdge', 'start, end, dist, maxspeed')    
# Read input
def readInput():
    # read speed limit for the city
    cityLimit = float(input())
    # read distances between vertices (and speed limits)
    distanceEdges = []
    inp = input()
    while (inp != ""):
        line = inp.split()
        v1 = line[0]
        v2 = line[1]
        dist = float(line[2])
        maxspeed = cityLimit
        if len(line)==4:
            maxspeed = float(line[3])
        # save values
        distanceEdges.append(DistanceEdge(v1,v2,dist,maxspeed))
        inp = input()
    # read speed measures (keep them in a dictionary)
    speedMeasures = {}
    inp = input()
    line = inp.split()
    while (len(line) > 1):
        # save input in a dict like: {v1:v2, list of speeds}
        # example: speedMeasures["aa:b"] = [12.5,11.3,10.2,15.3,12.0]
        speedMeasures[line[0]+":"+line[1]]=line[2:]
        inp = input()
        line = inp.split()
    # read source and destination vertices
    source = line[0]
    dest = input()
    return distanceEdges, speedMeasures, source, dest

# add value in list in dictionary (in a list way, because path is not hashable)
def addToListDic(dic,key,value):
	found = False
	for path,ts in dic:
		if key==path:
			dic.remove((path,ts))
			ts.append(value)
			dic.append((path,ts))
			found = True
	if found==False:
		dic.append((key,[value]))

# calculates mean value of a list
def mean(lst): 
    return sum(lst) / len(lst) 

# Read Input and call Dijkstra 100 times
def computesShortestPaths():
    distanceEdges,speedMeasures,source,dest = readInput()
    pathTimes = []
    random.seed(24)
    for i in range(100):
        edges = []
        for de in distanceEdges:
            # find each edge
            v1 = de.start
            v2 = de.end
            dist = de.dist
            speed = de.maxspeed
            # check if it has measures
            key = v1+":"+v2
            if key in speedMeasures:
                # select one randomly
                measures = speedMeasures[key]
                speed = float(random.choice(measures))
            # cost is the time it takes = dist/speed
            # if it is zero, time is inf
            cost = inf
            if speed!=0:
                cost = dist/speed
            # add it to the edges list
            edges.append((v1,v2,cost))

        graph = Graph(edges)
        time,path = graph.dijkstra(source,dest)
        time=time*60 # convert to minutes
        addToListDic(pathTimes,path,time)
    # compute mean time for all paths found
    means = [(inf,[]),(inf,[])]
    for p,ts in pathTimes:
    	m = mean(ts)
    	means.append((m,p))
    means.sort()
    # print the best two options
    print("{:.1f}".format(means[0][0]))
    print(means[0][1])
    print("{:.1f}".format(means[1][0]))
    print(means[1][1])

if __name__ == "__main__":
    computesShortestPaths()
