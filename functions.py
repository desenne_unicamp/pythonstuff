# author: Carolina de Senne Garcia
# RA 145656
# c145656@dac.unicamp.br
# desennecarol@gmail.com

import time
import datetime
import numpy as np

# criar uma lista com apenas os valores pares de outra lista
def onlyEven(L):
    return [x for x in L if x%2 == 0]

# criar uma lista com os valores nas posicoes pares
def onlyEvenPos(L):
    return [L[i] for i in range(len(L)) if i%2==0]

# criar um dicionario com a contagem de cada elemento numa lista
def frequency(L):
    dic = {}
    for e in L:
        if e in dic:
            dic[e]=dic[e]+1
        else:
            dic[e]=1
    return dic

# qual é a chave associada ao maior valor num dicionario (valores positivos)
def keyGreatestValue(dic):
    max, itm = 0, ''
    for k in dic:
        if dic[k] > max:
            max,itm = dic[k],k
    return itm

# qual o elemento mais comum numa lista
def mostFrequent(L):
    return keyGreatestValue(frequency(L))

# uma lista é sublista de outra
def isSublist(sub,L):
    tamS = len(sub)
    for i in range(len(L)-tamS):
        if L[i:i+tamS]==sub:
            return True
    return False

# dado 2 strings o fim de um é igual ao comeco do outro
def endIsBeginning(sf,sb):
    index = 1
    while index < min(len(sf),len(sb)):
        if sb[:index]==sf[0-index:]:
            return True
        index+=1
    return False

# dadas 2 strings, sao palindromos
def isPalindrome(s1,s2):
    fS1 = [c for c in s1 if c!=' ']
    fS2 = [c for c in s2 if c!=' ']
    rfS2 = fS2[::-1]
    return rfS2==fS1
    #if len(fS1) != len(fS2):
    #    return False
    #for i in range(len(fS1)):
    #    if fS1[i]!=fS2[len(fS2)-i-1]:
    #        return False
    #return True

##### Iterators

# pares: dado um iterator, retorna um iterator com os elementos nas posicoes pares (0,2,..)
def evenIterator(it):
    try:
        while True:
            x = next(it)
            yield x
            y = next(it)
    except StopIteration:
        pass

# reverte: dado um iterator, reverte ele
def revertIterator(it):
    l = list(it) # make list from iterator
    rl = l[::-1] # reverse list
    return (x for x in rl) # make iterator from list

# zip: dado 2 iterators, retorna um iterator que retorna os elementos intercalados
def zipIterators(it1,it2):
    try: # zip both
        while True:
            x1 = next(it1)
            yield x1
            x2 = next(it2)
            yield x2
    except StopIteration:
        pass
    try: # finish first
        while True:
            x1 = next(it1)
            yield x1
    except StopIteration:
        pass
    try: # finish second
        while True:
            x2 = next(it2)
            yield x2
    except StopIteration:
        pass

# cart: dado 2 iterators, retorna um iterator com o produto cartesiano dos elementos *
def cartIterators(it1,it2):
    try:
        while True:
            x1 = next(it1)
            x2 = next(it2)
            yield x2*x1
    except StopIteration:
        pass

# ciclo: dado um iterator, retorna os elementos num ciclo infinito
def infiniteIterator(it):
    l = list(it)
    while True:
        for x in l:
            yield x

# rangeinf(init,passo=1): retorna um iterator que gera numeros de init ate infinito, com passo
def rangeInfIterator(init,passo=1):
    x = init
    while True:
        yield x
        x += passo

# take: como o take do haskell (retorna os n primeiros valores do iterator)
def takeIterator(it,n):
    for i in range(n):
        try:
            x = next(it)
            yield x
        except StopIteration:
            pass

# drop - como o drop do haskell (retorna o iterator sem os n primeiros valores)
def dropIterator(it,n):
    for i in range(n):
        try:
            x = next(it)
        except StopIteration:
            pass
    try:
        while True:
            x = next(it)
            yield x
    except StopIteration:
        pass

# Implemente um iterator que recebe um iterator e retorna os elementos sem repetições do mesmo elemento em sequencia.
def noRepetitions(it):
    try:
        last = next(it)
        yield(last)
        while True:
            x = next(it)
            if x!=last:
                last = x
                yield(x)
    except StopIteration:
        pass


# Escreva um iterator filtra que recebe como entrada um iterator it e uma função d e 
# a) devolve o 1o elemento de it 
# b) devolve o proximo elemento de it (novo) que satisfaz d(novo,ultimo) onde ultimo é o ultimo elemento devolvido
# O iterator NAO pode expandir o iterator it desnecessariamente
def filtra(it,d):
    try:
        ultimo = next(it)
        yield(ultimo)
        while True:
            novo = next(it)
            if d(novo,ultimo):
                yield(novo)
            ultimo = novo
    except StopIteration:
        pass

# decorator para imprimir o tempo de execucao
def execTime(f):
    def wrapper(*args):
        ts = time.time()
        x = f(*args)
        te = time.time()
        t = (te - ts) * 1000
        print("execution time in miliseconds:",t)
        return x
    return wrapper

# decorator para construir um string com linhas para a hora e argumentos e saida de cada chamada da funcao. 
# O string sera acessado via atributo
class logLines:
    def __init__(self,f):
        self.f=f
        self.log=""
    def __call__(self,*args):
        dt = datetime.datetime.now()
        out = self.f(*args)
        line = str(dt.hour)+":"+str(dt.minute)+":"+str(dt.second)+": arguments: "+str(args)+", output: "+str(out)+"\n"
        self.log = self.log+line
        return out

# decorator para memoizar a funcao. 
# Memoizacao é criar um dicionario que se lembra dos valores de entrada e de saida da funcao ja executado. 
# Se um desses valores de entrada for re-executado, a funcao nao sera re-executada - 
# ela apenas retorna o valor de saida memoizado
class memoize:
    def __init__(self,f):
        self.f=f
        self.outputs = dict([])
    def __call__(self,*args):
        if args in self.outputs:
            return self.outputs[args]
        out = self.f(*args)
        self.outputs[args] = out
        return out

class descontinuada:
    def __init__(self,f,nome1,nome2):
        self.f=f
        self.firstTime = True
        self.n1 = nome1
        self.n2 = nome2
    def __call__(self,*args):
        if self.firstTime:
            message = "a funcao "+self.n1+" sera descontinuada na proxima edicao da biblioteca\nuse a funcao "+self.n2+" na proxima versão"
            print(message)
            self.firstTime = False
        return self.f(*args)

##### numpy stuff

# dado 1 array 1D, troque todos os valores > 0 para 1 e os < 0 para -1
def replace(a):
	a[a > 0] = 1
	a[a < 0] = -1
	return a

# Dado uma matriz, normalize as linhas de forma que a norma (soma dos quadrados) das linhas seja 1
def normalize(a):
	lineSum = np.sum(a,axis=1)
	divide =  a / lineSum.reshape((lineSum.size,1))
	sqrt = np.sqrt(divide)
	return sqrt

# normalize as linhas cujos elementos na primeira coluna são negativos
def normalizeNegative(a):
	lineSum = np.sum(a,axis=1)
	divide =  a / lineSum.reshape((lineSum.size,1))
	sqrt = np.sqrt(divide)
	columns = a[:,0] < 0
	a[columns] = sqrt[columns]
	return a

def maisRapido(a):
    mins = np.amin(a,axis=1)
    mins = mins.reshape((mins.size,1))
    index = [a <= mins*1.05]
    faster = np.zeros(a.shape)
    faster[tuple(index)]=1
    total = np.sum(faster,axis=0)
    return np.argmax(total)

def exceptionCreate():
    raise StopIteration

def exceptionCatch():
    try:
        while True:
            exceptionCreate()
    except StopIteration:
        print("hey, I caught this exception")
        pass
